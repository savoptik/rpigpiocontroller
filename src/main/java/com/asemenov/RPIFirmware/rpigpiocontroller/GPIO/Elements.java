package com.asemenov.rpifirmware.rpigpiocontroller.GPIO;

import java.util.*;

public class Elements {
    public static final int SPEACKER = 18;
    public static final int WITE = 24;
    public static final int BLUE = 25;
    public static final int GREEN = 12;
    public static final int YELLOW = 16;
    public static final int RED = 23;
    public static final int KEY1 = 11;
    public static final int KEY2 = 13;
    public static final int KEY3 = 26;
    public static final int KEY4 = 21;
    public static final int SWITCH = 17;
    public static final int BUTTON = 27;

    private static final ArrayList<Integer> INPUTS = new ArrayList(List.of(KEY1, KEY2, KEY3, KEY4, BUTTON, SWITCH));
    private static final ArrayList<Integer> OUTPUTS = new ArrayList<>(List.of(SPEACKER, BLUE, WITE, RED, YELLOW, GREEN));
    private static final ArrayList<Integer> PINS = new ArrayList<>(List.of(SPEACKER, BLUE, WITE, RED, YELLOW, GREEN, KEY1, KEY2, KEY3, KEY4, BUTTON, SWITCH));

    public static ArrayList<Integer> getPINS() {
        return PINS;
    }

    public static ArrayList<Integer> getINPUTS() {
        return INPUTS;
    }

    public static ArrayList<Integer> getOUTPUTS() {
        return OUTPUTS;
    }

    private static Map<Integer, Mode> modeMap = null;

    public static Mode getMode(final int pinNumber) {
        if (!PINS.contains(pinNumber)) return null;
        if (modeMap == null) {
            modeMap = new  HashMap();
            for (int i : INPUTS) {
                modeMap.put(i, Mode.INPUT);
            }
            for (int i :OUTPUTS) {
                modeMap.put(i, Mode.OUTPUT);
            }
        }
        return modeMap.get(pinNumber);
    }

    private static Map<String, Integer> PIN_NAMES_MAP = null;

    public static int GetPinOfName(final String name) {
        if (PIN_NAMES_MAP == null) {
            PIN_NAMES_MAP = Map.of("blue", BLUE,
                    "wite", WITE,
                    "red", RED,
                    "yellow", YELLOW,
                    "green", GREEN,
                    "speacker", SPEACKER);
            PIN_NAMES_MAP.put("key1", KEY1);
            PIN_NAMES_MAP.put("key2", KEY2);
            PIN_NAMES_MAP.put("key3", KEY3);
            PIN_NAMES_MAP.put("key4", KEY4);
            PIN_NAMES_MAP.put("switch", SWITCH);
            PIN_NAMES_MAP.put("button", BUTTON);
        }
        if (PIN_NAMES_MAP.containsKey(name)) {
            return PIN_NAMES_MAP.get(name);
        }
        return -1;
    }
}
