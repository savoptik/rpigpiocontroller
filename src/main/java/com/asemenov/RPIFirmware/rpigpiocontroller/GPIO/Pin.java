package com.asemenov.rpifirmware.rpigpiocontroller.GPIO;

import ch.qos.logback.classic.net.SyslogAppender;
import com.pi4j.Pi4J;
import com.pi4j.context.Context;
import com.pi4j.io.gpio.digital.DigitalOutput;
import com.pi4j.io.gpio.digital.DigitalState;
import com.pi4j.platform.Platforms;
import com.pi4j.util.Console;
import java.lang.reflect.InvocationTargetException;

public class Pin {
    private final int pin;
    private final Mode mode;
    private int value = 0;
    private String name;

    private static Context pi4j = null;

    static {
        pi4j = Pi4J.newAutoContext();
    }

    public Pin(final int p) {
        this(p, Elements.getMode(p), null);
    }

    public Pin(final int p, final Mode m, final String name) {
        this.pin = p;
        this.mode = m;
        this.name = name;
    }

    public Pin(final String name) {
        this(Elements.GetPinOfName(name), Elements.getMode(Elements.GetPinOfName(name)), name);
    }

    public int getPin() {
        return pin;
    }

    public Mode getMode() {
        return mode;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        try {
            System.out.println("Test 1, context is null = " + String.valueOf(pi4j == null));
            var ledConfig = DigitalOutput.newConfigBuilder(pi4j)
                    .id("led")
                    .name("LED Flasher")
                    .address(pin)
                    .shutdown(DigitalState.LOW)
                    .initial(DigitalState.LOW)
                    .provider("pigpio-digital-output");

            System.out.println("Test 2, ledConfig is null = " + String.valueOf(ledConfig == null));
            var led = pi4j.create(ledConfig);
            System.out.println("Test 2.1");
            System.out.println("Test 3, led is null = " + String.valueOf(led == null));
            if (value != 0) led.high();
            else led.low();
            this.value = value;
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            //e.printStackTrace();
        }
}

    public void process(final int timeOut) {
        process(new Runnable() {
            @Override
            public void run() {
                setValue(1);
                try {
                    Thread.sleep(timeOut);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                setValue(0);
            }
        });
    }

    public void process(Runnable run) {
        if (run != null) {
            new Thread(run).start();
        }
    }
}
