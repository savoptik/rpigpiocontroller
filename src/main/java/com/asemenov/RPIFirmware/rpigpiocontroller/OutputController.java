package com.asemenov.rpifirmware.rpigpiocontroller;

import com.asemenov.rpifirmware.rpigpiocontroller.GPIO.Pin;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/output")
public class OutputController {

    private static final String SUCCESS_STATUS = "success";
    private static final String ERROR_STATUS = "error";
    private static final int CODE_SUCCESS = 200;
    private static final int AUTH_FAILURE = 202;

    @GetMapping
    public BaseResponse showStatus() {
        return new BaseResponse(SUCCESS_STATUS, 1, "this is test");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/set")
    public BaseResponse setValueForPin(@RequestParam Map<String, String> customQuery) {
        String name =  customQuery.get("pin");
        if (name != null) {
            Pin pin = new  Pin(name);
            String val = customQuery.get("val");
            if (val != null) {
                Integer v = Integer.valueOf(val);
                if (v != null) {
                    pin.setValue(v);
                    return new BaseResponse(SUCCESS_STATUS, CODE_SUCCESS, name + " pin number: " + String.valueOf(pin.getPin()) + " value: " + String.valueOf(pin.getValue()));
                }
            }
        }

        return new BaseResponse(ERROR_STATUS, 102, "");
    }

}
