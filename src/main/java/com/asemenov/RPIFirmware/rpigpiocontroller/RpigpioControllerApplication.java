package com.asemenov.rpifirmware.rpigpiocontroller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RpigpioControllerApplication {

    public static void main(String[] args) {
        SpringApplication.run(RpigpioControllerApplication.class, args);
    }

}
